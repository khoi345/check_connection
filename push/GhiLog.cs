﻿using Microsoft.VisualBasic;

namespace ping_google
{
    public class GhiLog
    {
        private static object lockObject = new object();

        public void writeLog(string data, string filName)
        {
            try
            {
                DateTime dt = DateTime.Now;
                string directoryPath = "C:\\MyLog\\";
                string fullPath = directoryPath + dt.ToString("yyyy-MM-dd ") + filName;

                // Kiểm tra nếu thư mục không tồn tại thì tạo thư mục mới
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }

                // Sử dụng lock để đảm bảo chỉ một luồng được phép thực hiện ghi vào tệp tại một thời điểm.
                lock (lockObject)
                {
                    using (StreamWriter sw = (File.Exists(fullPath)) ? File.AppendText(fullPath) : File.CreateText(fullPath))
                    {
                        sw.WriteLine(data);
                    }
                }
            }
            catch (Exception ex)
            {
                // Xử lý ngoại lệ ở đây
                Console.WriteLine("Lỗi: " + ex.Message);
            }
        }
    }
}