﻿using Microsoft.Win32;
using Newtonsoft.Json;
using ping_google;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using PingPong;
namespace ping_google
{
  
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }
        private void RedirectConsoleOutput()
        {
            Console.SetOut(new ConsoleToRichTextBox(richTextBox1));
        }
        private async void frmMain_Load(object sender, EventArgs e)
        {
            ShowUngDungChayCungWin();
            RedirectConsoleOutput();
            // Đọc thông tin cấu hình từ RegistryKey
            string jsonText = "";// ReadConfigFromRegistry();
            if (string.IsNullOrEmpty(jsonText))
            {
                // không đọc được thì đọc từ config json
                jsonText = File.ReadAllText("config.json");
                if (jsonText != null)
                {
                    WriteConfigToRegistry(jsonText);
                }
            }
            Config configData = JsonConvert.DeserializeObject<Config>(jsonText);
            Console.WriteLine("Đọc thông tin cấu hình: {0}", JsonConvert.SerializeObject(configData));

            Conection.domain_name = configData.domain_name;
            Conection.minute_wait = configData.minute_wait;

            // Gọi phương thức SchedulePingJob từ đối tượng vừa tạo
            // await Ping.SchedulePingJob();
            CheckConnection p = new CheckConnection(Conection.minute_wait ,Conection.domain_name);

        }
        private string ReadConfigFromRegistry()
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("push_thong_tin");
            if (key != null)
            {
                return key.GetValue("ConfigKey").ToString();
            }
            return string.Empty;
        }
        private bool WriteConfigToRegistry(string cau_hinh)
        {
            try
            {
                // Mở hoặc tạo một SubKey với tên "push_thong_tin" trong phần CurrentUser của Registry
                using (RegistryKey key = Registry.CurrentUser.CreateSubKey("push_thong_tin"))
                {
                    if (key != null)
                    {
                        // Lưu giá trị cấu hình vào khóa "ConfigKeyName"
                        key.SetValue("ConfigKey", cau_hinh);
                        Console.WriteLine("Đã tạo Config trong Registry.");
                        return true; // Đã ghi thành công cấu hình vào Registry
                    }
                    else
                    {
                        // Xử lý khi không thể tạo hoặc mở được SubKey
                        Console.WriteLine("Không thể tạo hoặc mở SubKey trong Registry.");
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                // Xử lý khi có lỗi xảy ra trong quá trình ghi vào Registry
                Console.WriteLine("Lỗi khi ghi vào Registry: " + ex.Message);
                return false;
            }
        }
        private bool DeleteConfigFromRegistry()
        {
            try
            {
                // Mở hoặc tạo một SubKey với tên "push_thong_tin" trong phần CurrentUser của Registry
                using (RegistryKey key = Registry.CurrentUser.OpenSubKey("push_thong_tin", true))
                {
                    if (key != null)
                    {
                        // Xóa toàn bộ khóa và tất cả các giá trị con của nó
                        Registry.CurrentUser.DeleteSubKeyTree("push_thong_tin");
                        Console.WriteLine("Đã xóa Config từ Registry.");
                        return true; // Đã xóa thành công cấu hình từ Registry
                    }
                    else
                    {
                        // Xử lý khi không thể mở SubKey
                        Console.WriteLine("Không thể mở SubKey trong Registry để xóa.");
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                // Xử lý khi có lỗi xảy ra trong quá trình xóa khỏi Registry
                Console.WriteLine("Lỗi khi xóa từ Registry: " + ex.Message);
                return false;
            }
        }
        public void ShowUngDungChayCungWin()
        {
            using (RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true))
            {
                var value = key.GetValue("push_thong_tin");

                if (value != null)
                {
                    ckKhoiDongCungWin.Checked = true;
                    Console.WriteLine("Ứng dụng đang được thiết lập để khởi động cùng Windows.");
                }
                else
                {
                    ckKhoiDongCungWin.Checked = false;
                    Console.WriteLine("Ứng dụng không được thiết lập để khởi động cùng Windows.");
                }
            }
        }
        private void ckKhoiDongCungWin_CheckedChanged(object sender, EventArgs e)
        {
            string appPath = AppDomain.CurrentDomain.BaseDirectory + "push_thong_tin.exe";
            Console.WriteLine(appPath);
            if (ckKhoiDongCungWin.Checked)
            {
                // viết config khi cần chạy
                string jsonText = File.ReadAllText("config.json");
                if (jsonText != null)
                {
                    WriteConfigToRegistry(jsonText);
                }
                using (RegistryKey key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true))
                {
                    try
                    {
                        key.SetValue("push_thong_tin", appPath);
                        Console.WriteLine("Úng dụng đã được thiết lập để chạy cùng Windows.");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Đã xảy ra lỗi thiết lập chạy cùng Windows: " + ex.Message);
                    }
                }
            }
            else
            {
                DeleteConfigFromRegistry();
                using (RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true))
                {
                    try
                    {
                        key.DeleteValue("push_thong_tin");
                        Console.WriteLine("Ứng dụng đã được xóa khỏi danh sách khởi động cùng Windows.");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Đã xảy ra lỗi không thể xóa khỏi danh sách khởi động cùng Windows: " + ex.Message);
                    }
                }
            }
        }
    }
}