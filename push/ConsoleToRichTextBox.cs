﻿using System.Text;

namespace ping_google
{
    public class ConsoleToRichTextBox : TextWriter
    {
        private RichTextBox _richTextBox;
        private int lineCount = 0;

        public ConsoleToRichTextBox(RichTextBox richTextBox)
        {
            _richTextBox = richTextBox;
        }

        public override void Write(char value)
        {
            _richTextBox.Invoke(new Action(() => AppendText(value.ToString())));
        }

        public override void Write(string value)
        {
            _richTextBox.Invoke(new Action(() => AppendText(value)));
        }

        private void AppendText(string value)
        {
            _richTextBox.AppendText(value);
            lineCount++;

            if (lineCount >= 1000)
            {
                DeleteFirstLines(1); // Xóa 50 dòng cũ nhất
                lineCount -= 1; // Giảm số dòng đã xuất hiện
            }
        }

        private void DeleteFirstLines(int numberOfLines)
        {
            int firstIndex = _richTextBox.GetFirstCharIndexFromLine(numberOfLines);
            _richTextBox.Select(0, firstIndex);
            _richTextBox.SelectedText = "";
        }

        public override Encoding Encoding => System.Text.Encoding.UTF8;
    }
}