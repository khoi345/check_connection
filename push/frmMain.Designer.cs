﻿namespace ping_google
{
    partial class frmMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            richTextBox1 = new RichTextBox();
            ckKhoiDongCungWin = new CheckBox();
            SuspendLayout();
            // 
            // richTextBox1
            // 
            richTextBox1.Dock = DockStyle.Fill;
            richTextBox1.Location = new Point(0, 0);
            richTextBox1.Name = "richTextBox1";
            richTextBox1.Size = new Size(560, 202);
            richTextBox1.TabIndex = 0;
            richTextBox1.Text = "";
            // 
            // ckKhoiDongCungWin
            // 
            ckKhoiDongCungWin.AutoSize = true;
            ckKhoiDongCungWin.Dock = DockStyle.Bottom;
            ckKhoiDongCungWin.Location = new Point(0, 202);
            ckKhoiDongCungWin.Name = "ckKhoiDongCungWin";
            ckKhoiDongCungWin.Size = new Size(560, 19);
            ckKhoiDongCungWin.TabIndex = 1;
            ckKhoiDongCungWin.Text = "Khởi động cùng win";
            ckKhoiDongCungWin.UseVisualStyleBackColor = true;
            ckKhoiDongCungWin.CheckedChanged += ckKhoiDongCungWin_CheckedChanged;
            // 
            // frmMain
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(560, 221);
            Controls.Add(richTextBox1);
            Controls.Add(ckKhoiDongCungWin);
            Icon = (Icon)resources.GetObject("$this.Icon");
            Name = "frmMain";
            Text = "Ping google";
            Load += frmMain_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private RichTextBox richTextBox1;
        private CheckBox ckKhoiDongCungWin;
    }
}