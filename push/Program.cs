﻿namespace ping_google
{
    internal static class Program
    {
        static Form mainForm;
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // To customize application configuration such as set high DPI settings or default font,
            // see https://aka.ms/applicationconfiguration.
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            mainForm = new frmMain();
            mainForm.HandleCreated += MainForm_HandleCreated;
            Application.Run(mainForm);

        }
        private static void MainForm_HandleCreated(object sender, EventArgs e)
        {
            // Thực hiện các thao tác UI sau khi cửa sổ đã được tạo
            // Ví dụ: Invoke hoặc BeginInvoke
            if (mainForm.IsHandleCreated)
            {
                mainForm.BeginInvoke(new Action(() =>
                {
                    // Thực hiện các thao tác UI ở đây
                }));
            }
        }
    }
}