﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using FluentScheduler;
using System;
using System.Net.WebSockets;

namespace PingPong
{
    public class CheckConnection
    {
        private static bool _is_first_run = true;
        private static DateTime _next_runTime;
        private static int _minutes_wait = 1;
        private string _host = "google.com";


        public CheckConnection(int minutes_wait = 1,string host = "google.com")
        {
            _host = host;
            _minutes_wait = minutes_wait;
            // Đặt lịch trình cho công việc ping Google mỗi 5 phút
            JobManager.AddJob(() => RunJob(), s => s.ToRunEvery(minutes_wait).Minutes());

            // Chạy công việc một lần ngay từ đầu
            RunJob();
        }
        public async void RunJob()
        {
            Console.WriteLine("Running the job...");

            // Kiểm tra kết nối
            var x = await PingGoogle();

            // Đánh dấu là công việc đã chạy lần đầu
            _is_first_run = false;
            // Đặt thời gian cho lần chạy tiếp theo
            _next_runTime = DateTime.Now.AddMinutes(_minutes_wait);
            Console.WriteLine($"Next run in {Math.Ceiling((_next_runTime - DateTime.Now).TotalMinutes)} minutes.");

        }
        public async Task<bool> PingGoogle()
        {
            Ping ping = new Ping();

            try
            {
                PingReply reply = ping.Send(_host);
                if (reply != null && reply.Status == IPStatus.Success)
                {
                    Console.WriteLine($"Ping to {_host} successful. Roundtrip time: {reply.RoundtripTime}ms");
                    return true;
                }
                else
                {
                    Console.WriteLine($"Ping to {_host} failed.");
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error during ping: {ex.Message}");
                return false;
            }
        }
    }
}
